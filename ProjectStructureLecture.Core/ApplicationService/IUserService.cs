﻿using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.Entity;
using System.Collections.Generic;

namespace ProjectStructureLecture.Core.ApplicationService
{
    public interface IUserService
    {
        User CreateUser(NewUser user);
        User FindUserById(int id);
        List<User> GetAllUsers();
        User UpdateUser(int Id, NewUser userUpdate);
        User DeleteUser(int id);
    }
}
