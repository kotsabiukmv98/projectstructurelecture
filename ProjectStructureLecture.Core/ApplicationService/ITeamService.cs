﻿using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.Entity;
using System.Collections.Generic;

namespace ProjectStructureLecture.Core.ApplicationService
{
    public interface ITeamService
    {
        Team CreateTeam(NewTeam team);
        Team FindTeamById(int id);
        List<Team> GetAllTeams();
        Team UpdateTeam(int id, NewTeam teamUpdate);
        Team DeleteTeam(int id);
    }
}