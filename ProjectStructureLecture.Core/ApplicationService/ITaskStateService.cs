﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.Entity;

namespace ProjectStructureLecture.Core.ApplicationService
{
    public interface ITaskStateService
    {
        TaskState CreateTaskState(NewTaskState taskState);
        TaskState FindTaskStateById(int id);
        List<TaskState> GetAllTaskStates();
        TaskState UpdateTaskState(int id, NewTaskState taskStateUpdate);
        TaskState DeleteTaskState(int id);
    }
}
