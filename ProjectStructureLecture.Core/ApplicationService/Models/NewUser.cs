﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureLecture.Core.ApplicationService.Models
{
    public class NewUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int? TeamId { get; set; }
        public DateTime Birthday { get; set; }
    }
}
