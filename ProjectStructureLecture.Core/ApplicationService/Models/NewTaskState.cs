﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureLecture.Core.ApplicationService.Models
{
    public class NewTaskState
    {
        public string Value { get; set; }
    }
}
