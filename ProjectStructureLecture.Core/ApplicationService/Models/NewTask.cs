﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureLecture.Core.ApplicationService.Models
{
    public class NewTask
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}
