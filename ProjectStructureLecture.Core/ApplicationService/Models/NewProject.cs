﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureLecture.Core.ApplicationService.Models
{
    public class NewProject
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
    }
}