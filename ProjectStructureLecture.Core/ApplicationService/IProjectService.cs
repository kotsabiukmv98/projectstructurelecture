﻿using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.Entity;
using System.Collections.Generic;

namespace ProjectStructureLecture.Core.ApplicationService
{
    public interface IProjectService
    {
        Project CreateProject(NewProject project);
        Project FindProjectById(int id);
        List<Project> GetAllProjects();
        Project UpdateProject(int id, NewProject projectUpdate);
        Project DeleteProject(int id);
    }
}
