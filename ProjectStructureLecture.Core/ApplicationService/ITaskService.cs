﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.Entity;

namespace ProjectStructureLecture.Core.ApplicationService
{
    public interface ITaskService
    {
        Task CreateTask(NewTask task);
        Task FindTaskById(int id);
        List<Task> GetAllTasks();
        Task UpdateTask(int id, NewTask taskUpdate);
        Task DeleteTask(int id);
    }
}
