﻿using AutoMapper;
using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.DomainService;
using ProjectStructureLecture.Core.Entity;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructureLecture.Core.ApplicationService.Services
{
    public class TeamService : ITeamService
    {
        readonly ITeamRepository _teamRepo;
        readonly IMapper _mapper;

        public TeamService(ITeamRepository teamRepository, IMapper mapper)
        {
            _teamRepo = teamRepository;
            _mapper = mapper;
        }

        public Team CreateTeam(NewTeam newTeam)
        {
            var team = _mapper.Map<NewTeam, Team>(newTeam);
            return _teamRepo.Create(team);
        }

        public Team DeleteTeam(int id)
        {
            return _teamRepo.Delete(id);
        }

        public Team FindTeamById(int id)
        {
            return _teamRepo.ReadById(id);
        }

        public List<Team> GetAllTeams()
        {
            return _teamRepo.ReadAll().ToList();
        }

        public Team UpdateTeam(int id, NewTeam teamUpdate)
        {
            var team = _mapper.Map<NewTeam, Team>(teamUpdate);
            team.Id = id;
            return _teamRepo.Update(team);
        }
    }
}
