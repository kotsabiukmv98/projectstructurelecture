﻿using AutoMapper;
using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.DomainService;
using ProjectStructureLecture.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructureLecture.Core.ApplicationService.Services
{
    public class TaskService : ITaskService
    {
        readonly ITaskRepository _taskRepo;
        readonly IMapper _mapper;

        public TaskService(ITaskRepository taskRepository, IMapper mapper)
        {
            _taskRepo = taskRepository;
            _mapper = mapper;
        }

        public Task CreateTask(NewTask newTask)
        {
            var task = _mapper.Map<NewTask, Task>(newTask);
            if (task.State == 2 || task.State == 3)
                task.FinishedAt = DateTime.Now;

            return _taskRepo.Create(task);
        }

        public Task DeleteTask(int id)
        {
            return _taskRepo.Delete(id);
        }

        public Task FindTaskById(int id)
        {
            return _taskRepo.ReadById(id);
        }

        public List<Task> GetAllTasks()
        {
            return _taskRepo.ReadAll().ToList();
        }

        public Task UpdateTask(int id, NewTask newTaskUpdate)
        {
            var task = _mapper.Map<NewTask, Task>(newTaskUpdate);
            task.Id = id;

            if (task.State == 2 || task.State == 3)
                task.FinishedAt = DateTime.Now;

            return _taskRepo.Update(task);
        }
    }
}
