﻿using AutoMapper;
using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.DomainService;
using ProjectStructureLecture.Core.Entity;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructureLecture.Core.ApplicationService.Services
{
    public class UserService : IUserService
    {
        readonly IUserRepository _userRepository;
        readonly IMapper _mapper;
        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public User CreateUser(NewUser newUser)
        {
            var user = _mapper.Map<NewUser, User>(newUser);
            return _userRepository.Create(user);
        }

        public User DeleteUser(int id)
        {
            return _userRepository.Delete(id);
        }

        public User FindUserById(int id)
        {
            return _userRepository.ReadById(id);
        }

        public List<User> GetAllUsers()
        {
            return _userRepository.ReadAll().ToList();
        }

        public User UpdateUser(int Id, NewUser userUpdate)
        {
            var user = _mapper.Map<NewUser, User>(userUpdate);
            user.Id = Id;
            return _userRepository.Update(user);
        }
    }
}
