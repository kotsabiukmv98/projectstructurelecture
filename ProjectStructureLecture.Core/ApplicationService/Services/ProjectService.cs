﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.DomainService;
using ProjectStructureLecture.Core.Entity;

namespace ProjectStructureLecture.Core.ApplicationService.Services
{
    public class ProjectService : IProjectService
    {
        readonly IProjectRepository _projectRepo;
        readonly IMapper _mapper;

        public ProjectService(IProjectRepository projectRepository, IMapper mapper)
        {
            _mapper = mapper;
            _projectRepo = projectRepository;
        }

        public Project CreateProject(NewProject newProject)
        {
            var project = _mapper.Map<NewProject, Project>(newProject);
            return _projectRepo.Create(project);
        }

        public Project DeleteProject(int id)
        {
            return _projectRepo.Delete(id);
        }

        public Project FindProjectById(int id)
        {
            return _projectRepo.ReadById(id);
        }

        public List<Project> GetAllProjects()
        {
            return _projectRepo.ReadAll().ToList();
        }

        public Project UpdateProject(int id, NewProject projectUpdate)
        {
            var project = _mapper.Map<NewProject, Project>(projectUpdate);
            project.Id = id;
            return _projectRepo.Update(project);
        }
    }
}
