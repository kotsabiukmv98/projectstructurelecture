﻿using AutoMapper;
using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.DomainService;
using ProjectStructureLecture.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructureLecture.Core.ApplicationService.Services
{
    public class TaskStateService : ITaskStateService
    {
        readonly ITaskStateRepository _taskStateRepo;
        readonly IMapper _mapper;
        public TaskStateService(ITaskStateRepository taskStateRepository, IMapper mapper)
        {
            _taskStateRepo = taskStateRepository;
            _mapper = mapper;
        }

        public TaskState CreateTaskState(NewTaskState newTaskState)
        {
            var taskState = _mapper.Map<NewTaskState, TaskState>(newTaskState);
            return _taskStateRepo.Create(taskState);
        }

        public TaskState DeleteTaskState(int id)
        {
            return _taskStateRepo.Delete(id);
        }

        public TaskState FindTaskStateById(int id)
        {
            return _taskStateRepo.ReadById(id);
        }

        public List<TaskState> GetAllTaskStates()
        {
            return _taskStateRepo.ReadAll().ToList();
        }

        public TaskState UpdateTaskState(int id, NewTaskState taskStateUpdate)
        {
            var task = _mapper.Map<NewTaskState, TaskState>(taskStateUpdate);
            task.Id = id;
            return _taskStateRepo.Update(task);
        }
    }
}
