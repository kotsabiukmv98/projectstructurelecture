﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructureLecture.Core.Entity;

namespace ProjectStructureLecture.Core.DomainService
{
    public interface ITeamRepository
    {
        Team Create(Team team);
        Team ReadById(int id);
        IEnumerable<Team> ReadAll();
        Team Update(Team teamUpdate);
        Team Delete(int id);
    }
}
