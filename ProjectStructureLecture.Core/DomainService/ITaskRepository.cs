﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructureLecture.Core.Entity;

namespace ProjectStructureLecture.Core.DomainService
{
    public interface ITaskRepository
    {
        Task Create(Task task);
        Task ReadById(int id);
        IEnumerable<Task> ReadAll();
        Task Update(Task taskUpdate);
        Task Delete(int id);
    }
}
