﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructureLecture.Core.Entity;

namespace ProjectStructureLecture.Core.DomainService
{
    public interface IUserRepository
    {
        User Create(User customer);
        User ReadById(int id);
        IEnumerable<User> ReadAll();
        User Update(User userUpdate);
        User Delete(int id);
    }
}
