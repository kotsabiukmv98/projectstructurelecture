﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructureLecture.Core.Entity;

namespace ProjectStructureLecture.Core.DomainService
{
    public interface ITaskStateRepository
    {
        TaskState Create(TaskState taskState);
        TaskState ReadById(int id);
        IEnumerable<TaskState> ReadAll();
        TaskState Update(TaskState taskStateUpdate);
        TaskState Delete(int id);
    }
}
