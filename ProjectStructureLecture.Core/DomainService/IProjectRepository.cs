﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructureLecture.Core.Entity;

namespace ProjectStructureLecture.Core.DomainService
{
    public interface IProjectRepository
    {
        Project Create(Project project);
        Project ReadById(int id);
        IEnumerable<Project> ReadAll();
        Project Update(Project projectUpdate);
        Project Delete(int id);
    }
}
