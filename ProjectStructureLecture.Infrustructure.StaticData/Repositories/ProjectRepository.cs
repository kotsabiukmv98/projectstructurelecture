﻿using ProjectStructureLecture.Core.DomainService;
using ProjectStructureLecture.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructureLecture.Infrustructure.StaticData.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private static int _id = FakeDB.Projects.Count + 1;
        public Project Create(Project project)
        {
            project.Id = _id++;
            project.CreatedAt = DateTime.Now;
            
            FakeDB.Projects.Add(project);
            return project;
        }

        public Project Delete(int id)
        {
            var project = ReadById(id);
            if (project == null)
                return null;

            FakeDB.Projects.Remove(project);
            return project;
        }

        public IEnumerable<Project> ReadAll()
        {
            return FakeDB.Projects;
        }

        public Project ReadById(int id)
        {
            var project = FakeDB.Projects.FirstOrDefault(p => p.Id == id);

            return project;
        }

        public Project Update(Project projectUpdate)
        {
            var project = ReadById(projectUpdate.Id);
            if (project == null)
                return null;

            projectUpdate.CreatedAt = project.CreatedAt;
            project = projectUpdate;

            return project;
        }
    }
}
