﻿using ProjectStructureLecture.Core.DomainService;
using ProjectStructureLecture.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructureLecture.Infrustructure.StaticData.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        private static int _id = FakeDB.Tasks.Count + 1;

        public Task Create(Task task)
        {
            task.Id = _id++;
            task.CreatedAt = DateTime.Now;

            FakeDB.Tasks.Add(task);
            return task;
        }

        public Task Delete(int id)
        {
            var task = ReadById(id);
            if (task == null)
                return null;

            FakeDB.Tasks.Remove(task);
            return task;
        }

        public IEnumerable<Task> ReadAll()
        {
            return FakeDB.Tasks;
        }

        public Task ReadById(int id)
        {
            var task = FakeDB.Tasks.FirstOrDefault(t => t.Id == id);

            return task;
        }

        public Task Update(Task taskUpdate)
        {
            var task = ReadById(taskUpdate.Id);
            if (task == null)
                return null;

            taskUpdate.CreatedAt = task.CreatedAt;
            task = taskUpdate;

            return task;
        }
    }
}
