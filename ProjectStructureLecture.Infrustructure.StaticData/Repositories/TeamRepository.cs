﻿using ProjectStructureLecture.Core.DomainService;
using ProjectStructureLecture.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructureLecture.Infrustructure.StaticData.Repositories
{
    public class TeamRepository : ITeamRepository
    {
        private static int _id = FakeDB.Teams.Count + 1;

        public Team Create(Team team)
        {
            team.Id = _id++;
            team.CreatedAt = DateTime.Now;

            FakeDB.Teams.Add(team);
            return team;
        }

        public Team Delete(int id)
        {
            var team = ReadById(id);
            if (team == null)
                return null;

            FakeDB.Teams.Remove(team);
            return team;
        }

        public IEnumerable<Team> ReadAll()
        {
            return FakeDB.Teams;
        }

        public Team ReadById(int id)
        {
            var team = FakeDB.Teams.FirstOrDefault(t => t.Id == id);

            return team;
        }

        public Team Update(Team teamUpdate)
        {
            var team = ReadById(teamUpdate.Id);
            if (team == null)
                return null;

            teamUpdate.CreatedAt = team.CreatedAt;
            team = teamUpdate;

            return team;
        }
    }
}
