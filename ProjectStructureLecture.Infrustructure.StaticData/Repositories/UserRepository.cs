﻿using ProjectStructureLecture.Core.DomainService;
using ProjectStructureLecture.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructureLecture.Infrustructure.StaticData.Repositories
{
    public class UserRepository : IUserRepository
    {
        private static int _id = FakeDB.Users.Count + 1;

        public User Create(User user)
        {
            user.Id = _id++;
            user.RegisteredAt = DateTime.Now;
            
            FakeDB.Users.Add(user);
            return user;
        }

        public User Delete(int id)
        {
            var user = ReadById(id);
            if (user == null)
                return null;

            FakeDB.Users.Remove(user);
            return user;
        }

        public IEnumerable<User> ReadAll()
        {
            return FakeDB.Users;
        }

        public User ReadById(int id)
        {
            var user = FakeDB.Users.FirstOrDefault(u => u.Id == id);

            return user;
        }

        public User Update(User userUpdate)
        {
            var user = ReadById(userUpdate.Id);
            if (user == null)
                return null;
            userUpdate.RegisteredAt = user.RegisteredAt;
            user = userUpdate;
            return user;
        }
    }
}
