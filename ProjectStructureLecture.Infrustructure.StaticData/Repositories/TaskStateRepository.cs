﻿using ProjectStructureLecture.Core.DomainService;
using ProjectStructureLecture.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructureLecture.Infrustructure.StaticData.Repositories
{
    public class TaskStateRepository : ITaskStateRepository
    {
        private static int _id = FakeDB.TaskStates.Count + 1;

        public TaskState Create(TaskState taskState)
        {
            taskState.Id = _id++;

            FakeDB.TaskStates.Add(taskState);
            return taskState;
        }

        public TaskState Delete(int id)
        {
            var taskState = ReadById(id);
            if (taskState == null)
                return null;

            FakeDB.TaskStates.Remove(taskState);
            return taskState;
        }

        public IEnumerable<TaskState> ReadAll()
        {
            return FakeDB.TaskStates;
        }

        public TaskState ReadById(int id)
        {
            var taskState = FakeDB.TaskStates.FirstOrDefault(ts => ts.Id == id);

            return taskState;
        }

        public TaskState Update(TaskState taskStateUpdate)
        {
            var taskState = ReadById(taskStateUpdate.Id);
            if (taskState == null)
                return null;

            taskState.Value = taskStateUpdate.Value;

            return taskState;
        }
    }
}
