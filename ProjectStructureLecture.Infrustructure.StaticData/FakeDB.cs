﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using ProjectStructureLecture.Core.Entity;

namespace ProjectStructureLecture.Infrustructure.StaticData
{
    static class FakeDB
    {
        public static readonly IList<Project> Projects;
        public static readonly IList<Task> Tasks;
        public static readonly IList<TaskState> TaskStates;
        public static readonly IList<Team> Teams;
        public static readonly IList<User> Users;
        static FakeDB()
        {
            var currDir = Directory.GetCurrentDirectory() + "./bin/Debug/netcoreapp2.1/Data/";

            Projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText(currDir + "projects.json"));
            Tasks = JsonConvert.DeserializeObject<List<Task>>(File.ReadAllText(currDir + "tasks.json"));
            TaskStates = JsonConvert.DeserializeObject<List<TaskState>>(File.ReadAllText(currDir + "taskstates.json"));
            Teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText(currDir + "teams.json"));
            Users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText(currDir + "users.json"));
        }
    }
}
