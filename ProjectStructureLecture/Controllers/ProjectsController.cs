﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureLecture.Core.ApplicationService;
using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.Entity;

namespace ProjectStructureLecture.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        // GET: api/Projects
        [HttpGet]
        public ActionResult<IEnumerable<Project>> Get()
        {
            return _projectService.GetAllProjects();
        }

        // GET: api/Projects/5
        [HttpGet("{id}")]
        public ActionResult<Project> Get(int id)
        {
            var project = _projectService.FindProjectById(id);
            if (project == null)
            {
                return StatusCode(404, $"Did not find project with ID {id}");
            }
            return project;
        }

        // POST: api/Projects
        [HttpPost]
        public ActionResult<Project> Post([FromBody] NewProject project)
        {
            if (string.IsNullOrEmpty(project.Name))
            {
                return BadRequest("Name is required for creating Project");
            }

            return _projectService.CreateProject(project);
        }

        // PUT: api/Projects/5
        [HttpPut("{id}")]
        public ActionResult<Project> Put(int id, [FromBody] NewProject newProject)
        {
            var project = _projectService.UpdateProject(id, newProject);
            if (newProject == null)
            {
                return StatusCode(404, $"Did not find project with ID {id}");
            }

            return project;
        }

        // DELETE: api/Projects/5
        [HttpDelete("{id}")]
        public ActionResult<Project> Delete(int id)
        {
            var project = _projectService.DeleteProject(id);
            if (project == null)
            {
                return StatusCode(404, $"Did not find project with ID {id}");
            }

            return project;
        }
    }
}
