﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureLecture.Core.ApplicationService;
using ProjectStructureLecture.Core.ApplicationService.Models;
using Task = ProjectStructureLecture.Core.Entity.Task;

namespace ProjectStructureLecture.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        // GET: api/Tasks
        [HttpGet]
        public ActionResult<IEnumerable<Task>> Get()
        {
            return _taskService.GetAllTasks();
        }

        // GET: api/Tasks/5
        [HttpGet("{id}")]
        public ActionResult<Task> Get(int id)
        {
            var task = _taskService.FindTaskById(id);
            if (task == null)
            {
                return StatusCode(404, $"Did not find task with ID {id}");
            }
            return task;
        }

        // POST: api/Tasks
        [HttpPost]
        public ActionResult<Task> Post([FromBody] NewTask newTask)
        {
            if (string.IsNullOrEmpty(newTask.Name))
            {
                return BadRequest("Name is required for creating Task");
            }

            return _taskService.CreateTask(newTask);
        }

        // PUT: api/Tasks/5
        [HttpPut("{id}")]
        public ActionResult<Task> Put(int id, [FromBody] NewTask newTask)
        {
            //return Ok(_taskService.UpdateTask(id, newTask));
            var task = _taskService.UpdateTask(id, newTask);
            if (task == null)
            {
                return StatusCode(404, $"Did not find task with ID {id}");
            }

            return task;
        }

        // DELETE: api/Tasks/5
        [HttpDelete("{id}")]
        public ActionResult<Task> Delete(int id)
        {
            var task = _taskService.DeleteTask(id);
            if (task == null)
            {
                return StatusCode(404, $"Did not find task with ID {id}");
            }

            return task;
        }
    }
}
