﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureLecture.Core.ApplicationService;
using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.Entity;

namespace ProjectStructureLecture.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        // GET: api/Users
        [HttpGet]
        public ActionResult<IEnumerable<User>> Get()
        {
            return _userService.GetAllUsers();
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public ActionResult<User> Get(int id)
        {
            var user = _userService.FindUserById(id);
            if (user == null)
            {
                return StatusCode(404, $"Did not find user with ID {id}");
            }
            return user;
        }

        // POST: api/Users
        [HttpPost]
        public ActionResult<User> Post([FromBody] NewUser user)
        {
            if (string.IsNullOrEmpty(user.FirstName))
            {
                return BadRequest("FirstName is required for creating User");
            }
            if (string.IsNullOrEmpty(user.LastName))
            {
                return BadRequest("LastName is required for creating User");
            }
            if (string.IsNullOrEmpty(user.Email))
            {
                return BadRequest("Email is required for creating User");
            }

            return _userService.CreateUser(user);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public ActionResult<User> Put(int id, [FromBody] NewUser newUser)
        {
            var user = _userService.UpdateUser(id, newUser);
            if (user == null)
            {
                return StatusCode(404, $"Did not find user with ID {id}");
            }

            return user;
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public ActionResult<User> Delete(int id)
        {
            var user = _userService.DeleteUser(id);
            if (user == null)
            {
                return StatusCode(404, $"Did not find user with ID {id}");
            }

            return user;
        }
    }
}
