﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructureLecture.Core.ApplicationService;
using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.Entity;
using System.Collections.Generic;

namespace ProjectStructureLecture.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        // GET: api/Teams
        [HttpGet]
        public ActionResult<IEnumerable<Team>> Get()
        {
            return _teamService.GetAllTeams();
        }

        // GET: api/Teams/5
        [HttpGet("{id}")]
        public ActionResult<Team> Get(int id)
        {
            var team = _teamService.FindTeamById(id);
            if (team == null)
            {
                return StatusCode(404, $"Did not find team with ID {id}");
            }
            return team;
        }

        // POST: api/Teams
        [HttpPost]
        public ActionResult<Team> Post([FromBody] NewTeam newTeam)
        {
            if (string.IsNullOrEmpty(newTeam.Name))
            {
                return BadRequest("Name is required for creating Team");
            }
            return _teamService.CreateTeam(newTeam);
        }

        // PUT: api/Teams/5
        [HttpPut("{id}")]
        public ActionResult<Team> Put(int id, [FromBody] NewTeam newTeam)
        {
            var team = _teamService.UpdateTeam(id, newTeam);
            if (team == null)
            {
                return StatusCode(404, $"Did not find team with ID {id}");
            }

            return team;
        }

        // DELETE: api/Teams/5
        [HttpDelete("{id}")]
        public ActionResult<Team> Delete(int id)
        {
            var team = _teamService.DeleteTeam(id);
            if (team == null)
            {
                return StatusCode(404, $"Did not find team with ID {id}");
            }

            return team;
        }
    }
}
