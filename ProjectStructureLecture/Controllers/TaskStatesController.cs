﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructureLecture.Core.ApplicationService;
using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.Entity;
using System.Collections.Generic;

namespace ProjectStructureLecture.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController : ControllerBase
    {
        private readonly ITaskStateService _taskStateService;

        public TaskStatesController(ITaskStateService taskStateService)
        {
            _taskStateService = taskStateService;
        }

        // GET: api/TaskStates
        [HttpGet]
        public ActionResult<IEnumerable<TaskState>> Get()
        {
            return _taskStateService.GetAllTaskStates();
        }

        // GET: api/TaskStates/5
        [HttpGet("{id}")]
        public ActionResult<TaskState> Get(int id)
        {
            var taskState = _taskStateService.FindTaskStateById(id);
            if (taskState == null)
            {
                return StatusCode(404, $"Did not find task state with ID {id}");
            }
            return taskState;
        }

        // POST: api/TaskStates
        [HttpPost]
        public ActionResult<TaskState> Post([FromBody] NewTaskState newTaskState)
        {
            if (string.IsNullOrEmpty(newTaskState.Value))
            {
                return BadRequest("Value is required for creating TaskStatus");
            }

            return _taskStateService.CreateTaskState(newTaskState);
        }

        // PUT: api/TaskStates/5
        [HttpPut("{id}")]
        public ActionResult<TaskState> Put(int id, [FromBody] NewTaskState newTaskState)
        {
            var taskState = _taskStateService.UpdateTaskState(id, newTaskState);
            if (taskState == null)
            {
                return StatusCode(404, $"Did not find task state with ID {id}");
            }

            return taskState;
        }

        // DELETE: api/TaskStates/5
        [HttpDelete("{id}")]
        public ActionResult<TaskState> Delete(int id)
        {
            var taskState = _taskStateService.DeleteTaskState(id);
            if (taskState == null)
            {
                return StatusCode(404, $"Did not find task state with ID {id}");
            }

            return taskState;
        }
    }
}
