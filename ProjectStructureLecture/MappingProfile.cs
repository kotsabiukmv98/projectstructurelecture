﻿using AutoMapper;
using ProjectStructureLecture.Core.ApplicationService.Models;
using ProjectStructureLecture.Core.Entity;
using Task = ProjectStructureLecture.Core.Entity.Task;

namespace ProjectStructureLecture
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<NewTeam, Team>();
            CreateMap<NewUser, User>();
            CreateMap<NewProject, Project>();
            CreateMap<NewTask, Task>();
            CreateMap<NewTaskState, TaskState>();
        }
    }
}
